import gulp from "gulp";
import cp from "child_process";
import gutil from "gulp-util";
import path from "path";
import scss from "gulp-sass";
import postcss from "gulp-postcss";
import pcssNested from "postcss-nested";
import pcssSyntax from "postcss-scss";
import purify from "gulp-purifycss";
import cssnext from "postcss-cssnext";
import cssnano from "cssnano";
import sourcemaps from "gulp-sourcemaps";
import svgstore from "gulp-svgstore";
import svgmin from "gulp-svgmin";
import inject from "gulp-inject";

import BrowserSync from "browser-sync";
import webpack from "webpack";
import webpackConfig from "./webpack.conf";

const browserSync = BrowserSync.create();
const hugoBin = `./bin/hugo.${process.platform === "win32" ? "exe" : process.platform}`;
const defaultArgs = ["-d", "../dist", "-s", "site", "-v"];
const previewArgs = ["--buildDrafts", "--buildFuture"];

// Development tasks
if (process.env.DEBUG) {
  defaultArgs.unshift("--debug")
}

gulp.task("hugo", (cb) => buildSite(cb) );
gulp.task("hugo-preview", (cb) => buildSite(cb, previewArgs) );

// Build/production tasks
gulp.task("build", ["js", "cms-assets", "cms-css", "redirects"], (cb) => buildSite(cb, [], "production", "css") )
gulp.task("build-preview", ["js", "cms-assets", "cms-css", "redirects"], (cb) => buildSite(cb, previewArgs, "production", "css") )

// Compile SCSS, optimise and minify with PostCSS
gulp.task("css", () => {
  gulp.src("./src/scss/main.scss")
    .pipe(sourcemaps.init())
    .pipe(scss.sync({
      paths: [
        path.join(__dirname, "scss", "includes")
      ]
    }).on("error", scss.logError))
    .pipe(postcss([
      pcssNested(),
      cssnext()
    ], { syntax: pcssSyntax }))
    // TODO: change js path to dist/
    .pipe(purify(['./src/js/**/*.js', './dist/**/*.html'], {
      whitelist: [ '*:not*', 'lazy', 'lazyloaded' ]
    }))
    .pipe(postcss([ cssnano({ autoprefixer: false }) ]))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("./dist/css"))
    .pipe(browserSync.stream())
});

gulp.task("cms-css", () => (
  gulp.src("./node_modules/netlify-cms/dist/cms.css")
    .pipe(postcss([
      cssnext(),
      cssnano({ autoprefixer: false })
    ]))
    .pipe(gulp.dest("./dist/css"))
));

gulp.task("cms-assets", () => (
  gulp.src("./node_modules/netlify-cms/dist/*.{woff,eot,woff2,ttf,svg,png}")
    .pipe(gulp.dest("./dist/css"))
));

// Compile Javascript
gulp.task("js", (cb) => {
  const myConfig = Object.assign({}, webpackConfig);

  webpack(myConfig, (err, stats) => {
    if (err) throw new gutil.PluginError("webpack", err);
    gutil.log("[webpack]", stats.toString({
      colors: true,
      progress: true
    }));
    browserSync.reload();
    cb();
  });
});

// Inline and compress SVG
gulp.task("svg", () => {
  const svgs = gulp
    .src(["site/static/img/**/*.svg", "!site/static/img/icons/spinner.svg", "!site/static/img/safari-pinned-tab.svg"]) // Spinner was causing HX logo to spin!!??
    .pipe(svgmin())
    .pipe(svgstore({inlineSvg: true}));

  function fileContents(filePath, file) {
    return file.contents.toString();
  }

  return gulp
    .src("site/layouts/partials/svg.html")
    .pipe(inject(svgs, {transform: fileContents}))
    .pipe(gulp.dest("site/layouts/partials/"));
});

gulp.task("redirects", () => {
  gulp.src("_redirects")
    .pipe(gulp.dest("./dist/"))
})

// Development server with browsersync
gulp.task("server", ["css", "hugo-preview", "cms-assets", "cms-css", "js", "svg"], (cb) => {
  // gulp.start("css");
  browserSync.init({
    ghostMode: false,
    server: {
      baseDir: "./dist"
    },
    notify: {
      styles: {
        top: "auto",
        bottom: "0"
      }
    }
  })
  gulp.watch("./src/js/**/*.js", ["js"]);
  gulp.watch(["./src/scss/**/*.scss", "!./src/scss/critical.scss"], ["css"]);
  gulp.watch("./site/static/img/**/*.svg", ["svg"]);
  gulp.watch("./site/**/*", ["hugo-preview"]);
})

/**
 * Run hugo and build the site
 */
function buildSite(cb, options, environment = "development", task) {
  const args = options ? defaultArgs.concat(options) : defaultArgs;

  process.env.NODE_ENV = environment;

  return cp.spawn(hugoBin, args, {stdio: "inherit"}).on("close", (code) => {
    if (code === 0) {
      if (task) gulp.start(task) // TODO: not needed if using gulpSequence now?
      browserSync.reload();
      cb();
    } else {
      browserSync.notify("Hugo build failed :(");
      cb("Hugo build failed");
    }
  });
}
