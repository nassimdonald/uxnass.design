---
id: holiday-extras
title: Holiday Extras
description: International travel company and UK market leader for travel extras.
intro: >-
  International travel company and the UK market leader for travel extras - with
  core products including airport hotels, parking, lounges, rail and coach
  transport, car hire, and holiday insurance. Arrangements for **seven million
  travellers** are made every year, with over 90% of bookings through their
  website.
featured: true
keywords: ["suggested"]
weight: 1
project_date: '2018-01-31'
logo: /img/hx-logo-horizontal@2x.png
color: hx-purple
bg_color: bg-hx-purple
bg_image: /img/hx-bg-purple@2x.jpg
project_icon: /img/hx-app-icon@2x.png
project_icon_large: /img/hx-app-icon-large@2x.png
feature_image:
  alt: Holiday Extras homepage
  image: /img/hx-project-feature-image@2x.jpg
product_image1:
  alt: Holiday Extras hotel availability page on tablet
  image: /img/hx-ipad-availability.png
product_image2:
  alt: Holiday Extras hotel page on mobile
  image: /img/hx-iphone-hotel-page-top.png
role: >-
  I worked alongside two other designers and over fifty developers, providing
  UX/UI support to the entire business.
project_output:
  - output: Branding assets and themes
  - output: Front-end UI development
  - output: Information architecture
  - output: Interaction design
  - output: Pattern library
  - output: User experience design
  - output: User interface design
  - output: Visual design
blocks:
  - heading: Humble beginnings
    text: >-
      When I began working with Holiday Extras, the business was still
      relatively new to the concept of user experience. Along with one other
      designer and three developers, my recruitment marked the beginning of a
      dedicated, in-house UX/UI design team. Faced with a massive codebase, a
      hugely fragmented approach to UI design and development across the
      business, and no single team taking ownership of the end-to-end customer
      journey, design was very much a free-for-all.
  - image_block:
      images:
        - alt: Holiday Extras hotel availability page
          caption: Hotel availability page redesign
          image: /img/hx-availability-laptop-hx@2x.jpg
      layout: single
    width: wide
  - heading: Diving in the deep end
    text: "Shortly after joining the newly-formed UX/UI team, I was jumping straight into the codebase, helping to build the foundations of an extensive pattern library and theming system for use across the entire business. The goal was to create the blueprint for UI development and a “single point of truth” for each brand.\n\n> Read more about [building Pattern Library](/article/pattern-library/)\n\nThe project was a monumental undertaking that required careful consideration. Lucky for me, I love to sweat the details and relish a challenge! \U0001F913"
  - image_block:
      images:
        - alt: Various mobile UI screens
          image: /img/hx-iso-misc-mobile-screens-hx@2x.jpg
          rellax: 2
    width: full
  - heading: Put your money where your mouse is
    text: >-
      During the development of the HX pattern library, the UX/UI team were
      juggling various other projects with more specific design problems to
      solve. The ability to build interactive prototypes in-browser streamlined
      the design and development workflow, and proved very effective in
      user-testing. These projects gave us the opportunity to put our pattern
      library into practice.


      Wireframes, site flows, user flow diagrams – everything was so much more
      focused knowing that the UI was a non-issue.
  - image_block:
      images:
        - alt: 'Misc. wireframes, site flows, and user flow diagrams.'
          caption: 'Misc. wireframes, site flows, and user flow diagrams.'
          image: /img/hx-misc-wireframes@2x.jpg
          pswp: true
    width: wide
  - class: bg-near-white pv1 ph5-m ph6-l mb0
    id: hx-misc-prototypes-gallery
    image_block:
      images:
        - alt: Manage your booking platform in various brand themes
          caption: >-
            Various brand themes applied to the post-booking platform for
            clients of Shortbreaks – subsidiary of Holiday Extras.
          image: /img/hx-sbmb-home-screens@2x.jpg
        - alt: JetParks homepage mockup and "Help & Support" modal mockup
          caption: Mockups for JetParks and a "Help & Support" modal screen.
          image: /img/hx-help-support-and-jetparks-mockups@2x.jpg
        - alt: Holiday Extras homepage redesign (top)
          caption: Holiday Extras homepage - redesigned and rebranded.
          image: /img/hx-homepage-top@2x.jpg
      layout: single
    width: full
  - class: bg-light-gray mt0
    id: hx-landing-pages-grid
    image_block:
      images:
        - image: /img/hx-homepage-full@2x.png
          pswp: true
        - image: /img/hx-landing-top@2x.png
          pswp: true
        - image: /img/hx-footer-full@2x.png
          pswp: true
      layout: grid
      columns: 3
    width: full
  - image_block:
      images:
        - alt: Hotel booking flow on multiple devices
          caption: >-
            The hotel booking flow was redesigned to work seamlessly on all
            devices.
          image: /img/hx-hotel-availability-devices@2x.jpg
      layout: single
    width: wide
  - heading: Hotel booking flow
    text: >-
      One of the bigger projects that benefited from the in-browser approach to
      prototyping was the redesign of the hotel booking flow. The project began
      with a series of research activities, such as extensive user testing with
      focus groups, data gathering from the existing site flow, and competitive
      analysis. Once we had identified the main pain points in the flow and
      highlighted areas for improvement, I began wireframing (loads of) ideas to
      get the ball rolling.
  - image_block:
      images:
        - alt: Hotel flow wireframes
          caption: Wireframes for the hotel flow redesign
          image: /img/hx-hotel-flow-wireframes-master@2x.jpg
    width: wide
  - image_block:
      images:
        - autoplay: true
          caption: Walkthrough of the brand new hotel product page.
          video: /img/hx-hotel-page-walkthrough-hx.mp4
    text: >-
      Using InVision to create simple prototypes from the wireframes, it was
      very easy to get a quick idea of how the pieces of the new flow might fit
      together. As always, it was key to get feedback as soon as possible, so
      the prototypes were shared amongst colleagues in the office and a small
      selection of users outside the business.
  - image_block:
      images:
        - alt: Alternative hotel product page design (top)
          caption: Alternative design for the top of the hotel product page
          image: /img/hx-hotel-page-v1-top@2x.jpg
          pswp: true
  - text: >-
      The designs performed well, but given the product-focused nature of the
      project, it became evident from this exercise that a more high-fidelity,
      data-driven prototype would be needed to get a better sense of how the
      designs would perform with actual hotel data. Using the pattern library,
      Handlebars templating, and a crude JSON file of hotel data, I built a
      series of interactive prototypes based on the wireframes.
  - class: bg-near-white ph6-l
    image_block:
      images:
        - alt: Alternative design for hotel availability page
          caption: Alternative design for the hotel availability page.
          image: /img/hx-hotel-availability-alt-hx@2x.jpg
          pswp: true
        - alt: Various mobile screens of the hotel product page.
          caption: Sections of the hotel product page on mobile.
          image: /img/hx-hotel-page-mobile-screens@2x.jpg
          pswp: true
      layout: grid
    width: full
  - heading: Product selection flows
  - text: >-
      This dynamic approach to prototyping was very useful in materialising the
      various designs for a product selection flow. For some of these concepts,
      animations were used in testing the effectiveness of varying degrees of
      progressive disclosure.
  - class: mv0
    image_block:
      images:
        - alt: Product selection prototype - modal variation
          autoplay: true
          column_class: w-70-ns
          video: /img/hx-product-selection-modal.mp4
        - alt: Product selection prototype - modal variation on mobile
          aspect_ratio: 9x16
          autoplay: true
          column_class: w-30-ns
          frame: iphone8
          video: /img/hx-product-selection-mobile-modal.mp4
      layout: grid
    width: wide
  - class: mv0
    image_block:
      images:
        - alt: Product selection prototype - drawer variation
          autoplay: true
          column_class: w-70-ns
          video: /img/hx-product-selection-drawer.mp4
        - alt: Product selection prototype - drawer variation on mobile
          aspect_ratio: 9x16
          autoplay: true
          column_class: w-30-ns
          frame: iphone8
          video: /img/hx-product-selection-mobile-drawer.mp4
      layout: grid
    width: wide
  - class: mv0
    image_block:
      images:
        - alt: Product selection prototype - panes variation
          autoplay: true
          column_class: w-70-ns
          video: /img/hx-product-selection-panes.mp4
        - alt: Product selection prototype - panes variation on mobile
          aspect_ratio: 9x16
          autoplay: true
          column_class: w-30-ns
          frame: iphone8
          video: /img/hx-product-selection-mobile-panes.mp4
      layout: grid
    width: wide
  - heading: Travel timeline
    image_block:
      images:
        - alt: "Travel timeline example"
          image: /img/hx-timeline-to-airport-detail-hx@2x.jpg
    text: "One of my feature ideas for a travel timeline also benefited hugely from data-driven prototyping. The timeline intially aimed to solve the problem of communicating the sometimes complex logistical nature of hotel with parking products. However, as the design evolved, its many uses in other cases such as email itineraries and the company’s Trip Planner platform became apparent."
  - image_block:
      images:
        - alt: Travel timeline walkthrough
          autoplay: true
          video: /img/hx-timeline-walkthrough-hx.mp4
    text: "Given the complexity and sheer magnitude of the data to consider in this design, I was rather proud of how it all came together and how the prototype held up with even the most awkward edge cases – e.g. airport hotel before departure with Meet & Greet parking on return. \U0001F92F"
  - heading: Reflection
    text: >-
      My time at Holiday Extras challenged me to further develop my skills in
      ways I hadn’t expected. The fruits of my labour helped the business
      address a number of fundamental UX and UI issues across various
      touchpoints, some of which saw revenue generation in the millions (GBP)
      after continual efforts to increase conversion rates. But commercial
      successes aside, I feel a real sense of accomplishment in having brought
      my design skills and expertise to a business where it was clearly needed.
  - heading: 'More, you say?'
    text: >-
      As you can imagine, this is just the tip of the iceberg. For a deeper
      insight into the work I did with Holiday Extras, check out my article on
      building the pattern library, or visit the Pattern Library itself.*


      <p class="silver"><small class="db">* Note: in the interest of design integrity and for your convenience, I have self-hosted a “snapshot” of Pattern Library,
      which does not necessarily represent the current content and state of the
      official live Holiday Extras Pattern Library.</small></p>
related_links:
  - label: Read next
    title: Building Pattern Library
    description: >-
      The monumental undertaking of building a pattern library for over twenty
      brands.
    feature_image: /img/hx-iso-misc-pl-screens@0.5x.jpg
    url: /article/pattern-library/
  - label: Demo
    title: Pattern Library in action
    description: >-
      For brevity and demonstration's sake, I have included a small subset of
      themes and content.
    feature_image: /img/hx-pl-homepage-thumb@2x.jpg
    url: http://hx-pattern-library.netlify.com
---
