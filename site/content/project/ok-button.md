---
id: ok-button
title: "OK Button"
description: Up and coming Scottish dream pop band, featuring yours truly. Coming soon…
featured: true
weight: 3
date: 2018-07-03T12:46:12+01:00
project_date: '2018-07-31'
wip: true
logo: /img/okb-logo-horizontal.svg
bg_image: /img/okb-bg-gradient.jpg
project_icon: /img/okb-app-icon@2x.png
project_icon_large: /img/okb-app-icon-large@2x.png
feature_image:
  alt: OK Button group photo
  image: /img/okb-project-feature-image.jpg
product_image1:
  alt: OK Button's 'The Message' video on iPhone
  image: /img/okb-iphone-video.png
product_image2:
  alt: OK Button vinyl artwork
  image: /img/okb-sepia-spectres-vinyl.png
  media_type: vinyl
---
