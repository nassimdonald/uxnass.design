---
id: salesseek
title: "SalesSeek"
description: London-based startup, providing sales & marketing software for B2B businesses.
intro: >-
  SalesSeek is an ambitious London-based startup, providing an all-in-one business platform for growing B2B businesses. It’s a CRM, Marketing Automation Platform, and Support Tools, all rolled into one software solution.
featured: true
keywords: ["suggested"]
weight: 2
project_date: '2015-03-01'
logo: /img/ss-logo-horizontal@2x.png
color: ss-blue
bg_color: bg-ss-blue
bg_image: /img/ss-bg-gradient.jpg
project_icon: /img/ss-app-icon@2x.png
project_icon_large: /img/ss-app-icon-large@2x.png
feature_image:
  image: /img/ss-project-feature-image.jpg
  alt: SalesSeek homepage
product_image1:
  image: /img/ss-ipad-contact-detail.png
  alt: SalesSeek contacts list and detail on tablet
product_image2:
  image: /img/ss-iphone-funnel-selected.png
  alt: SalesSeek deals funnel on mobile
role: >-
  As lead designer, I managed and mentored a team of three designers, responsible for all aspects of design, in addition to front-end development and architecture.
project_output:
  - output: Branding assets and themes
  - output: Front-end UI development
  - output: Information architecture
  - output: Interaction design
  - output: Pattern library
  - output: User experience design
  - output: User interface design
  - output: Visual design
blocks:
  - heading: Starting at the beginning…
    text: >-
      I was the second to be hired at SalesSeek, spearheading all aspects of design and front-end development from the get go. Having previously designed and built websites, I was already comfortable writing HTML, CSS, and snippets of JS here and there. But my role at SalesSeek demanded a much deeper knowledge of front-end development than I was prepared for. Getting up to scratch with JavaScript, learning the ins and outs of cutting-edge MVC frameworks (at the time), and understanding front-end development paradigms, it was a steep learning curve for a humble pixel-pusher.


      I relished the challenge. Within the first month, I had learned enough JS to start fleshing out the front-end and materialising some of the early design mockups and prototypes.
  - image_block:
      images:
        - image: /img/ss-person-detail-devices@2x.jpg
        - image: /img/ss-funnel-devices@2x.jpg
      layout: single
    width: wide
  - heading: Solid foundations
    text: >-
      Initially, SalesSeek was built as a web app. Many of the target users were familiar and comfortable with the paradigms of browser-based applications, so this naturally defined the blueprint for user-experience.


      Given the data-heavy nature of the application, I initially placed a large focus on creating rock-solid components – tables, lists and forms etc. – to provide the building blocks of the UI. All of this was augmented by extensive research of conventions in web design, of course, all whilst quickly discovering the beautiful and not-so-beautiful sides of JS.
  - image_block:
      images:
        - image: /img/ss-webapp-screens1@2x.jpg
        - image: /img/ss-webapp-screens2@2x.jpg
      layout: single
    width: wide
  - text: >-
      Within the first six months I had become proficient in a wide variety of modern web frameworks, libraries and plugins – Backbone, Underscore, Marionette, RequireJS, d3, Handsontables, TinyMCE, Select2, nanoScroller, and -prefix-free, to mention a few. It took a lot of stretch-testing to ensure we were investing in tools that were fit for purpose. With scalability in mind, LESS was the CSS pre-processor of choice, and Handlebars for HTML templating.
  - heading: Brand evolution
    text: >-
      In parallel to development, I dedicated time to creating a brand that encompassed the app and the company’s vision. Some of the concepts materialised in the app gave impetus to the evolution of the brand identity.
  - image_block:
      images:
        - image: /img/ss-old-contacts-sketches@2x.jpg
          caption: Early wireframe and mockup of the contacts section
        - image: /img/ss-activity-feed-versions@2x.jpg
          caption: Various visual design iterations of the activity feed
        - image: /img/ss-web-dashboard-old-ui@2x.jpg
          caption: Visual design experimentation with the web analytics section
        - image: /img/ss-old-web-visits-charts@2x.jpg
          caption: Early mockups of the web visits chart.
      layout: single
    width: wide 
  - text: >-
      I experimented with a few different visual styles whilst designing new app features and struggled to find an identity that felt right for the business. Then along came the bold stylistic changes of iOS 7, which changed the game. Jumping on the bandwagon, I felt inspired by the simplicity and vivid style of the new OS and quickly evolved the visual style to keep up with the growing trends in visual design.
  - image_block:
      images:
        - image: /img/ss-www-landing-devices@2x.jpg
          caption: Landing pages and iOS app introduction
    width: wide
  - text: >-
      A custom icon set was created to effectively communicate some of the concepts presented in the app and compliment the simplicity of the visual design. 
  - image_block:
      images:
        - image: /img/ss-custom-icons@2x.jpg
          caption: A selection from the custom-built icon set
        - image: /img/ss-logo-grayscale@2x.jpg
          caption: The SalesSeek logo was designed with the iOS7 Icon Grid
        - image: /img/ss-logo-blueprint@2x.jpg
        - image: /img/ss-ios-app-icon-template@2x.jpg
      layout: grid
      class: mw5 mw7-m mw9-l center
      columns: 4
    width: full
  - heading: Many features… and more!
    text: >-
      The sheer depth of functionality and range of features in SalesSeek made for some truly exciting development. Designing and building so many different features, components, interactions, and marrying it all together in a seamless experience; it was a huge learning experience and paved the way for a whole new level of UX/UI for me.
  - class: bg-ss-gradient-light pv1
    image_block:
      columns: 3
      images:
        - caption: Add notes to contacts and deals and keep track of latest activity, such as calls, emails, and sales progress.
          title: Activity feed
          image: /img/ss-activity@2x.jpg
        - caption: Extensive filtering and grouping functionality to "slice-and-dice" contact and deals.
          title: Grouping & filtering
          image: /img/ss-groups@2x.jpg
        - caption: Simple functionality for importing contacts and deals, and mapping to fields in the system.
          title: Bulk import
          image: /img/ss-import@2x.jpg
        - caption: Quick and easy task management and collaboration – integrated with contacts and deals.
          title: Task management
          image: /img/ss-tasks@2x.jpg
        - caption: Hook up a registration form on a website to capture leads and automatically import into SalesSeek.
          title: Web form mapping
          image: /img/ss-mapping@2x.jpg
        - caption: Dedicated cloud storage for managing and sharing files related to contacts, deals, and campaigns.
          title: File storage and sharing
          image: /img/ss-content@2x.jpg
      layout: grid
    width: full
  - text: >-
      Every feature went through an intensive process of wireframing, prototyping, and user-testing before reaching the app. Utilising my newly-developed coding skills, I prototyped many features in-browser, using the desired technology. This allowed me to fine-tune the UX more than prototyping software would allow. Often the prototype code would provide the basis for the production code.
  - image_block:
      images:
        - image: /img/ss-filters-process@2x.jpg
          caption: The process for designing list filters involved a lot of high-fidelity prototyping in the browser.
    width: wide
  - heading: Dashboards for all
    text: >-
      One of the biggest challenges of working on SalesSeek was designing different workflows for a variety of users. It was never going to be a one-size-fits-all solution, as our different user profiles expressed very different use cases. The solution was to create numerous dashboards tailored to the specifications of different roles. Combined with team structures and permissions functionality, this quickly uncovered very complex design problems to solve.
  - image_block:
      images:
        - image: /img/ss-webapp-dashboards1@2x.jpg
        - image: /img/ss-webapp-dashboards2@2x.jpg
      layout: single
    width: wide
  - heading: Dynamic data visualisation
    image_block:
      alignment: right
      images:
        - image: /img/ss-pipeline1@2x.jpg
          caption: Pipeline widget
        - image: /img/ss-pipeline2@2x.jpg
          caption: Pipeline widget
      layout: single
    text: "The web app boasts an impressive collection of visualisations designed to display data intelligently, informatively and beautifully. Visuals aside, I placed a lot of emphasis on providing useful and meaningful ways of interacting with the data. After all, data visualisations shouldn’t just be pretty to look at, they should be functional too! \U0001F485"
  - text: >-
      The pipeline widget is a good example of when it made more sense to be sparing with visualisation. In some instances, users were more concerned with number-crunching than visualising the data.


      The Campaign widget was particularly enjoyable to design. I made use of bold, contrasting colours and simple block forms to really draw the eye to the 'good news' – big splashes of colour communicate very effectively when a campaign is going well. The interactivity of this widget allowed users highlighting finer details in the data, and also made for some satisfying eye-candy!
  - bg_image: /img/ss-bg-gradient.jpg
    class: pa3 pa4-ns relative bg-ss-overlay-dark
    image_block:
      images:
        - image: /img/ss-campaign-widget1@2x.png
        - image: /img/ss-campaign-widget2@2x.png
        - image: /img/ss-campaign-widget3@2x.png
        - image: /img/ss-campaign-widget4@2x.png
        - image: /img/ss-campaign-widget5@2x.png
        - image: /img/ss-campaign-widget6@2x.png
        - image: /img/ss-campaign-widget7@2x.png
      layout: single
    inverse: true
    width: full
  - heading: The student becomes the teacher
    text: >-
      Once the wheels were in motion and the business had gradually started to expand, I naturally adopted the role of lead designer, responsible for all aspects of design while managing and mentoring a team of three designers. Inevitably, there was a degree of mentoring involved with the new developer recruits too, most of which had never worked with the likes of Backbone before.
  - heading: New beginnings
    text: >-
      Soon after the web app was released and the business had secured a number of key clients, development began on the mobile app. This new chapter presented an opportunity to address some existing UX issues in the web app by starting on a relatively clean slate.
  - bg_image: /img/ss-ios-wireframes-master@2x.jpg
    bg_overlay: bg-gradient-black-bottom
    class: mt4 mb6 mt5-ns nl3 nr3 nl4-ns nr4-ns relative pt6 pt7-ns
    image_block:
      images:
        - image: /img/ss-deal-detail-tabs@2x.png
          class: center pt3-ns relative bottom--2
          replace_class: true
      class: relative bottom--2 pt6 pt7-ns
    width: full
    rellax: 4
    replace_class: true
  - image_block:
      images:
        - image: /img/ss-ios-mockup-master@2x.jpg
          caption: The entire iOS app was wireframed and mocked up in Photoshop and imported to InVision for testing.
    text: >-
      With the knowledge gained from designing and building the web app, I set out to simplify and refine the user flows and improve usability, whilst maintaining a clear view of the overall product vision. This process was intensive, focusing on user research, competitive analysis, user profiling, process flow diagrams, wireframing and prototyping with InVision.
  - text: >-
      Designing for one device is a challenge in itself, but sometimes with a broader scope, inspiration can be found from design patterns on other devices. For example, the multi-column layout for the web app was initially designed with mobile and tablet devices in mind. The ultimate goal was to provide a seamless and consistent user experience across all devices.


      The SalesSeek iPhone App offers a subset of the features of the web app, but was designed to deliver so much more. By streamlining and simplifying the UI, the workflow of an average salesperson on the move was significantly improved. A number of the key features of the web app were reimagined for the mobile app, with greater emphasis on usability and productivity.
  - image_block:
      images:
        - image: /img/ss-misc-iphones-angled@2x.jpg
    width: wide
  - heading: A journey to remember
    text: >-
      My time at SalesSeek was my first taste of the fast-paced world of London tech startups. It was certainly trial by fire, having never built software before but through all the blood, sweat and tears of building SalesSeek from the ground up - okay, maybe just a bit of sweat - the reward of hearing how much users appreciated the design made it all worthwhile.
  - text: >-
      > “We chose SalesSeek over other CRMs due to the highly visual user experience, which allows instantly useful feedback that no other platform was providing.”
      
      <cite>Kevin B., President AT AgriWebb</cite>
  - text: >-
      > “It's not clunky like many alternatives, it's intuitive, easy to use, and pleasant to look at!”

      <cite>Harry K., Community Host</cite>
  - text: >-
      > “Their UX is one-of-a-kind, and having spent weeks reviewing many vendors, SalesSeek outdoes many of the bigger players; both in terms of simplicity, automation triggers and comprehensiveness.”

      <cite>Elliott D., Growth Specialist</cite>
  - text: >-
      > “My staff find it easy and intuitive rather than overbearing and complex. Absolutely love the visual nature of SalesSeek and especially the "Deal" section. It can deal with a large number and still seem manageable compared to long lists.”

      <cite>Tony D.</cite>
trusted_by: 
  companies:
    - name: ResDiary
      id: res-diary
    - name: Husky finance
      id: husky-finance
    - name: BIO
      id: bio
    - name: UNILAD
      id: unilad
    - name: Bink or miss out
      id: bink
    - name: Westfield
      id: westfield
  sprite: /img/ss-trusted-by-sprite@2x.jpg
---

{{< trusted_by >}}
