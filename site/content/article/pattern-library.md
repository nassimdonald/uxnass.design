---
title: Building Pattern Library
description: The monumental undertaking of building a pattern library for over twenty brands.
keywords: ["suggested"]
project_date: '2018-01-31'
logo: /img/hx-logo-horizontal@2x.png
logo_and_title: true
color: hx-purple
bg_color: bg-hx-purple
bg_image: /img/hx-bg-purple@2x.jpg
project_icon: /img/hx-app-icon@2x.png
project_icon_large: /img/hx-app-icon-large@2x.png
blocks:
  - heading: Diving in the deep end
    text: >-
      Shortly after joining the newly-formed UX/UI team, I was jumping straight into the codebase, helping to build the foundations of an extensive pattern library and theming system for use across the entire business. The goal was to create the blueprint for UI development and a “single point of truth” for each brand.


      The pattern library provided the framework for building UI across an enormous collection of sites. This worked hand-in-hand with the theming system for powerful over-branding functionality, serving internal brands and external clients, such as easyJet, British Airways, Thomas Cook, TUI, Money Supermarket, National Express, Saga, and Merlin (Alton Towers, Chessington, Legoland, Paultons, Thorpe Park).
    image_block:
      images:
        - alt: Button loading and complete states
          autoplay: true
          caption: Adding a pinch of iteraction design to Bootstrap's buttons
          video: /img/hx-button-loading-complete-states-hx.mp4
  - text: >-
      The project was a monumental undertaking that required careful consideration. Lucky for me, I love to sweat the details and relish a challenge! 🤓
  - image_block:
      images:
        - alt: Various screenshots from Pattern Library
          image: /img/hx-iso-misc-pl-screens@2x.jpg
          rellax: 2
    width: full
  - heading: Scaling Standards
    text: >-
      One of the biggest challenges of building this framework was introducing and maintaining a high standard of UI whilst scaling the solution to fit the needs of the business and its various clients. Fortunately the business had already been using Twitter Bootstrap in some capacity, which meant that LESS was already baked into the front end. The powerful functionality that LESS brought allowed us to scale to serve an infinite number of brands, whilst maintaining design quality and integrity at the foundation.
  - image_block:
      images:
        - alt: Product search form patterns
          caption: Product search form patterns
          image: /img/hx-search-forms-hx@2x.jpg
    width: wide
  - heading: Minding the gap
    text: >-
      Although Bootstrap provided an adequate starting point for our Pattern Library, we quickly realised there were gaps to be filled (literally and figuratively). While we were busy building and documenting UI patterns and brand themes, the Bootstrap framework itself was continually evolving. Switching to another framework or building our own would have been an enormous overhaul of the existing front-end with hundreds of live sites to refactor. This was a major constraint which felt like a deal breaker at first.


      > "Design is the art of gradually applying constraints until only one solution remains."

      <cite>Unknown author</cite>


      Once again, LESS to the rescue! By building a layer between Bootstrap and the themes that overrode its variables and components, we were able to extend Bootstrap and gradually add or replace parts from the library as necessary. For instance, Bootstrap didn’t have a date-picker component, so we were able to add one to our base theme for all themes to use and apply their branding to automatically - magic! As we developed the base theme, the changes were instantly applied to all themes, ultimately allowing us to control dozens of brands and UI across thousands of live pages from one place.
  - class: bg-near-white pv1-ns ph5-m ph6-l
    image_block:
      images:
        - alt: Footer pattern in various brand themes
          caption: Footer pattern in various brand themes
          image: /img/hx-pl-footers@2x.jpg
    width: full
  - heading: "\"Mobile-friendlifying\" Bootstrap"
    text: >-
      Bootstrap’s lack of consideration for mobile devices with components such as modals, dropdowns, popovers, and navbars was a real headache in many projects. So I took the liberty of creating a few extension classes and some custom JS to solve that problem and make the mobile experience a happier one.
  - image_block:
      images:
        - aspect_ratio: 9x16
          autoplay: true
          frame: iphone8
          video: /img/hx-homepage-menu-mobile-hx.mp4
  - heading: Great power, great responsibility
    text: >-
      With the enormous scale of this system, the importance of quality in our work was not to be taken lightly. One small mistake could have far-reaching concequences. That’s where the pattern library became as much a tool for design and documentation as it was for testing during theme development. Boasting a huge range of patterns and prototypes, the library provided the means to battle-test our designs and ensure we were iterating responsibly.
  - image_block:
      images:
        - alt: Navbar pattern with various brand themes applied
          image: /img/hx-navbars-all-brands@2x.jpg
          rellax: 1
    width: full
  - text: >-
      A great example of this was my creation of a set of utilities called Background Variants, which aimed to provide the means for creating styled sections of content with a branded palette of colours. Many CSS frameworks lack this functionality, mostly due to the sheer complexity of the “cascading” nature of CSS. Nonetheless, after hours of rigorous testing in many different UI cases and across evert theme, the solution was robust enough to rollout without any breaking changes to themes that hadn’t yet adopted the utility. The powerful potential for a much more visually appealing, branded experience that Background Variants presented became immediately evident.
  - image_block:
      images:
        - alt: Panel variations page in Pattern Library, with various brand themes applied.
          caption: Panel variations page with various brand themes applied.
          image: /img/hx-panels-screens-all-brands.jpg
    width: wide
  - text: >-
      With only three UX/UI designers – two senior; one junior – in the whole business (😓), naturally many design decisions fell on the developers who often felt ill-equipped to shoulder the responsibility. In addition to maintaining a high standard of work, it was also important to educate the team of seventy-odd developers on how to use utilise the tools we were creating, advocating the correct use of Bootstrap, encouraging best practices in HTML and LESS, and sharing our design knowledge. The pattern library provided the means for developers to employ design principles in their work in the absence of dedicated designer support.
    image_block:
      images:
        - autoplay: true
          caption: Walkthrough of the account management platform prototype
          video: /img/hx-sbmb-walkthrough-alton.mp4
  - heading: Two birds with one pattern
    text: >-
      Since the business had essentially built one platform to serve many of its products, there were a number of design problems solved with the work on the hotel flow that benefited other booking flows. Thus much of the work from the hotel flow project was consolidated into the pattern library for other use cases.
  - class: bg-hx-gray pv1-ns
    image_block:
      images:
        - alt: "Before: old product tile design"
          caption: The old, messy product card design
          image: /img/hx-parking-availability-tiles-old@2x.jpg
          title: Before
        - alt: "After: new product tile redesign"
          caption: The new and improved product card design
          image: /img/hx-hotel-availability-tiles-new@2x.jpg
          title: After
      layout: grid
    width: full
  - heading: Product list
    text: >-
      My creation of a flexible pattern for displaying lists of products was a much needed addition to the library. The design featured a number of improvements, such as an responsive and adaptive layout, mobile-friendly interaction, configurable horizontal and vertical layout styles, coloured rating scale, product logos, and varying list sizes and visual styles. This pattern would be used across a large range of products, from airport parking and hotels, to theme park packages and car hire.
  - image_block:
      images:
        - alt: Product list pattern
          image: /img/hx-product-lists-1@2x.jpg
        - alt: Product list pattern
          image: /img/hx-product-lists-2@2x.jpg
        - alt: Product list pattern
          image: /img/hx-product-lists-3@2x.jpg
        - alt: Product list pattern
          image: /img/hx-product-lists-4@2x.jpg
        - alt: Product list pattern
          image: /img/hx-product-lists-5@2x.jpg
      layout: single
    width: wide
  - heading: Navigation panes
    text: >-
      Whilst testing various ideas in the hotel flow, there was a need for a simple way to navigate between a stack of views (or “panes”) without detaching from the primary flow. I couldn’t find a plugin to do the job, so I wrote one myself! \**adds open-sourcing to the to-do list*\*
  - image_block:
      images:
        - aspect_ratio: 9x16
          autoplay: true
          frame: iphone8
          video: /img/hx-timeline-address-entry-mobile-hx.mp4
  - heading: More, you say?
    text: >-
      As you can imagine, this is just the tip of the iceberg. For a deeper insight into the work I did with [Holiday Extras](/project/holiday-extras/), check out the Pattern Library demo.*

      <p class="silver"><small class="db">* Note: in the interest of design integrity and for your convenience, I have self-hosted a “snapshot” of Pattern Library,
      which does not necessarily represent the current content and state of the
      official live Holiday Extras Pattern Library.</small></p>
related_links:
  - label: Demo
    title: Pattern Library in action
    description: >-
      For brevity and demonstration's sake, I have included a small subset of
      themes and content.
    feature_image: /img/hx-pl-homepage-thumb@2x.jpg
    url: http://hx-pattern-library.netlify.com
    target: _blank
---
