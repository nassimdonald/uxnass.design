import React from "react";
import CMS from "netlify-cms";
// import c from 'classnames';

const ObjectControl = CMS.getWidget('object').control
const ListControl = CMS.getWidget("list").control

const BlockButton = props => {
  const { field, onClick } = props
  const className = "flex-auto ma1 pv1 ph2 br2 bn gray bg-light-gray hover-bg-near-white"

  return (
    <button
      className={className}
      onClick={() => onClick(field)}>
      { field.get('name') }
    </button>
  )
}

export class BlockSelect extends ObjectControl {
  constructor(props) {
    super(props)
    this.onClick = this.onClick.bind(this)
  }

  onClick(widget) {
    console.log(widget)
    this.props.onChange(widget)
  }

  render() {
    const { field, value } = this.props;
    const { collapsed } = this.state;
    const fields = field.get('fields');

    if (value && value.size > 0) return this.controlFor(value)

    return (
      <div className="flex flex-wrap pa1 br2 br--bottom bg-moon-gray w-100">
        { collapsed ? null : fields.map((f, idx) => {
          return <BlockButton
            { ...this.props }
            field={f}
            key={idx}
            onClick={e => this.onClick(e)}
          />
        }) }
      </div>
    );
  }
}

export default class BlocksControl extends ListControl {

}
