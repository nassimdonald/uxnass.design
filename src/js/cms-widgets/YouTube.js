import React from "react"

const YouTube = {
  // Internal id of the component
  id: "youtube",
  // Visible label
  label: "Youtube",
  // Fields the user need to fill out when adding an instance of the component
  fields: [
    {name: 'id', label: 'Youtube Video ID', widget: 'string'},
    {name: 'autoplay', label: 'Autoplay', widget: 'boolean'}
  ],
  // Pattern to identify a block as being an instance of this component
  pattern: /{{< ?youtube ?((?: +(?:\w+)="(?:.*?)")+|(?:\S+)) ?>}}/,
  // Function to extract data elements from the regexp match
  fromBlock: function(match) {
    var reg = /(?:(?: +(\w+)="(.*?)")|(\S+))/g
    var result;
    var attr = {}
    while((result = reg.exec(match))) {
      attr[result[1]] = result[2]
    }
    return attr
  },
  // Function to create a text block from an instance of this component
  toBlock: function(obj) {
    const keys = Object.keys(obj)
    const attr = keys.length > 1 ? keys.map((key, i) => {
      if (obj[key]) return key + '="' + obj[key] + '" '
    }).join('') : obj[keys[0]]
    return attr && '{{< youtube ' + attr + ' >}}' || null;
  },
  // Preview output for this component. Can either be a string or a React component
  // (component gives better render performance)
  toPreview: function(obj) {
    return (
      <img src={"http://img.youtube.com/vi/"+(obj.id)+"/maxresdefault.jpg"} alt="YouTube Video" />
    );
  }
}

export default YouTube