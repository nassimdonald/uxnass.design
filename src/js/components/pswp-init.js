import $ from 'jquery'
// Core JS file
import PhotoSwipe from 'photoswipe'
// UI JS file
import PhotoSwipeUI_Default from 'PhotoSwipeUI_Default'
// Affixed navbar singleton
import { navAffix } from './nav-affix'

class PSWP {

  constructor($el) {
    var that = this

    $('html').removeClass('pswp-active')

    this.items = [] // array of slide objects that will be passed to PhotoSwipe()
    this.$figures = $el.find('.pswp-figure')

    if (this.photoSwipe) this.photoSwipe.destroy()

    // for every figure element on the page:
    this.$figures.each( function() {
      // get properties from child a/img/figcaption elements,
      var $figure = $(this)
      var $a      = $figure.find('a')
      var $src    = $a.attr('href')
      var $title  = $figure.find('figcaption').html()
      var $msrc   = $figure.find('img').attr('src')
      // if not, set temp default size then load the image to check actual size
      var item = {
        src   : $src,
        w     : 800, // temp default size
        h     : 600, // temp default size
        title : $title,
        msrc  : $msrc
      }
      // if data-size on <a> tag is set, read it and create an item
      if ($a.data('width') && $a.data('height')) {
        item.w = $a.data('width')
        item.h = $a.data('height')
      } else {
        // load the image to check its dimensions
        // update the item as soon as w and h are known (check every 30ms)
        var img = new Image()
        img.src = $src
        var wait = setInterval(function() {
          var w = img.naturalWidth
          var h = img.naturalHeight
          if (w && h) {
            clearInterval(wait)
            item.w = w
            item.h = h
          }
        }, 30)
      }
      // Save the index of this image then add it to the array
      var index = that.items.length
      that.items.push(item)
      // Event handler for click on a figure
      $figure.on('click', function(e) {
        e.preventDefault() // prevent the normal behaviour i.e. load the <a> hyperlink
        that.openPhotoSwipe(index)
      })
    })

    // Parse URL and open gallery if it contains #&pid=3&gid=1
    var hashData = this.photoswipeParseHash()
    if (hashData.pid && hashData.gid)
      this.openPhotoSwipe( hashData.pid, true, true)
  }

  getThumbBoundsFn(index) {
    // find thumbnail element
    var thumbnail = $(this.$figures[index]).find('img')[0]

    // get window scroll Y
    var pageYScroll = window.pageYOffset || document.documentElement.scrollTop
    // optionally get horizontal scroll

    // get position of element relative to viewport
    var rect = thumbnail.getBoundingClientRect()

    // w = width
    return { x:rect.left, y:rect.top + pageYScroll, w:rect.width }

    // Good guide on how to get element coordinates:
    // http://javascript.info/tutorial/coordinates
  }

  photoswipeParseHash() {
    var hash = window.location.hash.substring(1)
    var params = {}

    if (hash.length < 5) return params // pid=1

    var vars = hash.split('&')
    for (var i = 0; i < vars.length; i++) {
      if (!vars[i]) continue
      var pair = vars[i].split('=')
      if (pair.length < 2) continue
      params[pair[0]] = pair[1]
    }

    if (params.gid) params.gid = parseInt(params.gid, 10)

    return params
  }

  openPhotoSwipe(index, disableAnimation, fromURL) {
    // Get the PSWP element and initialise it with the desired options
    var $pswp = $('.pswp')[0]
    var options = {
      index: index,
      bgOpacity: 0.8,
      // Buttons/elements
      captionEl: false,
      counterEl: false,
      fullscreenEl: false,
      shareEl: false,
      zoomEl: false,
      // showAnimationDuration: 1000,
      // hideAnimationDuration: 1000,
      getThumbBoundsFn: (index) => this.getThumbBoundsFn(index)
    }

    if (!$pswp) return

    if (fromURL) {
      if (options.galleryPIDs) {
        // parse real index when custom PIDs are used
        // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
        for(var j = 0; j < this.items.length; j++) {
          if(this.items[j].pid == index) {
            options.index = j
            break
          }
        }
      } else {
        options.index = parseInt(index, 10) - 1
      }
    } else {
      options.index = parseInt(index, 10)
    }

    // exit if index not found
    if ( isNaN(options.index) ) return

    if (disableAnimation) options.showAnimationDuration = 0

    this.photoSwipe = new PhotoSwipe($pswp, PhotoSwipeUI_Default, this.items, options)

    // Hack to prevent PhotoSwipe from triggering onpopstate page transition
    $('html').addClass('pswp-active')

    navAffix.disable()

    this.photoSwipe.listen('close', () => navAffix.enable())

    this.photoSwipe.init()
  }
}

export default PSWP
