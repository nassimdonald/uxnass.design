// Vendor
import $ from 'jquery'
import anime from 'animejs'
import AOS from 'aos'

// TODO: consider a better parallax library with mobile support
// Rellax bugs! Less than -3 (i.e. -4, -5 etc.) causes choppiness on mobile
// Master build has bug with jumpy scrolling on iOS Safari when browser toolbar is shown/hidden
// import Rellax from 'rellax'
// Min JS with fix from https://dixonandmoe.com/rellax/
import Rellax from './components/rellax-fix'

// Modules
import VHChromeFix from './components/mobile-chrome-vh-fix'
import { navAffix } from './components/nav-affix'
import Modal from './components/modal'
import photoSwipe from './components/pswp-init'
import Util from 'util'

const $html               = $('html')
const $body               = $('body')
const pageDuration        = 400
const pageEasing          = 'easeOutCubic'
const pageInClasses       = 'hidden fixed absolute--fill overflow-hidden shadow-2 z-200 in'
const pageOutClasses      = 'page bg-white overflow-hidden out'
const currentYear         = new Date().getFullYear()
//
let instance = null

// App init
class App {

  constructor(config) {
    if (instance) instance = this

    this._req           = null
    this.maxDocHeight   = null
    this.PSWP           = null
    this.rellax         = null
    this.sr             = null
    this.state          = {}
    this.$ajaxContainer = $(config.ajaxContainer)

    return instance
  }

  init() {

    $(document).ready(() => this._onDocReady())

    $body.on('click', 'a[href="#"]', (e) => e.preventDefault())

    $body.on('blur', '.input-with-label', (e) => this._toggleInputValue(e.currentTarget))

    this._initMenu()

    // Set the intial state of history to the first loaded page
    if (typeof(history.replaceState) !== "undefined") {
      history.replaceState({ url: "" + window.location.pathname + "" }, null, null)
    }

    // Browser doesn't support HTML 5 history
    if (history.pushState) this._initAjax()

    return this
  }

  _showLoader(cb) {
    const $loader = $('#ajax-loader')
    $loader.show()
    $loader.find('.loader').show()
    if (cb) cb()
  }

  _hideLoader(animate, cb) {
    const $loader = $('#ajax-loader')
    $loader.find('.loader').hide()
    if (animate) {
      const loaderAnime = anime({
        duration: pageDuration*2,
        targets: $loader[0],
        opacity: 0,
        easing: 'linear',
        complete: () => {
          $loader.hide().css('opacity', '')
          if (cb) cb()
        }
      })
    }
    else {
      $loader.hide()
      if (cb) cb()
    }
  }

  _onDocReady() {
    const $loader = $('#ajax-loader')
    const $menuToggle = $('.menu-toggle')
    const $menu = $('#menu')
    const $page = $('.page.active')

    this._beforePageShow($page)

    this._hideLoader(true, () => {
      this._onPageShown($page)
      this._initAOS()
      $loader.removeClass('z-9999')
    })

    this._VHChromeFix()

    function toggleMenu(dialog, cb) {
      const { anim } = this
      if (anim) anim.pause()
      const prog = anim && !anim.completed ? (anim.progress/100) : 1
      const duration = 200 * prog
      const show = this._isShown

      this.anim = anime({
        targets: dialog,
        translateX: show ? [(prog * 100 + '%'),'0%'] : '100%',
        duration: show ? duration * 1.2 : duration,
        easing: show ? 'easeOutCirc' : 'easeOutQuad',
        complete: an => {
          $(dialog).css('translateX', '')
          if (cb) cb()
        }
      })
    }

    $menu
      .modal({
        animateIn: toggleMenu,
        animateOut: toggleMenu,
        show: false
      })
      .on('show.nd.modal', () => {
        $menuToggle.addClass('close silver').removeClass('color-inherit')
        // navAffix.disable()
      })
      .on('hide.nd.modal', () => {
        $menuToggle.addClass('color-inherit').removeClass('close silver')
        // navAffix.enable()
      })

    // this.maxDocHeight = Util.getViewportHeight()
    this.maxDocHeight = Math.max($(document).height(), $(window).height())

    // Debugging without animation
    // $loader.hide()
    // $html.removeClass('loading')
    // this._onPageShown()
    // this._initAOS()
  }

  /* ==========================================================================
     Menus
     ========================================================================== */

  _highlightMenuItem() {
    const sel = 'a[href="'+window.location.pathname+'"]'
    const $link = $('.menu-item '+sel)

    $link.parent().addClass('active')
    $('.menu-item a').not(sel).parent().removeClass('active')

    $('.sub-menu').each(function() {
      const active = $(this).has($(sel)).length > 0

      $(this)
        .toggleClass('active expanded', active)
        .find('> a .caret')
        .toggleClass('rotate-90', active)
        .end()
        .find('> .sub')
        .toggleClass('dn', !active)
    })
  }

  _toggleSubMenu($sm, show, cb) {
    const { anim } = $sm[0]
    const duration = 400
    const menu = $sm.find('>.sub')[0]

    if (show && $sm.hasClass('expanded')) return

    $sm
      .toggleClass('expanded', show)
      .find('> a .caret').toggleClass('rotate-90', show)

    if (anim) anim.pause()

    const prog = anim && !anim.completed ? (anim.progress/100) : 1
    const h1 = menu.style.height || 0
    if (show) {
      menu.style.height = null
      menu.style.opacity = 0
    }
    const h2 = $(menu).show().outerHeight()

    $sm[0].anim = anime({
      targets: menu,
      opacity: {
        value: show ? 1 : 0,
        duration: duration * prog,
        easing: show ? 'easeInCirc' : 'easeOutCirc'
      },
      height: {
        value: show ? [h1,h2] : 0,
        duration: duration * prog,
        easing: 'easeOutCirc'
      },
      begin: () => $(menu).addClass('overflow-hidden'),
      complete: an => {
        if (!show) $(menu).hide()
        menu.style.height = null
        menu.style.opacity = null
        $(menu).removeClass('overflow-hidden')
        if (cb) cb()
      }
    })
  }

  _initMenu() {
    $body.on('keydown', '.sub-menu a', (e) => {
      let $sm = $(e.currentTarget).closest('.sub-menu')
      if (e.keyCode === 37) { // Left
        $sm = $(e.currentTarget).closest('.sub-menu.expanded')
        if (!$sm.length) return
        this._toggleSubMenu($sm, false, () => $sm.find('> a').focus())
      } else if (e.keyCode === 39) { this._toggleSubMenu($sm, true) } // Right
    })

    $body.on('click', '.sub-menu > a', (e) => {
      const $sm = $(e.currentTarget).parent()
      e.preventDefault()
      this._toggleSubMenu($sm, !$sm.hasClass('expanded'))
    })

    $body.on('click', '#menu a:not([href="#"])', () => $('#menu').modal('hide'))
  }

  /* ==========================================================================
     100vh alternative in JS
     ========================================================================== */

  _VHChromeFix() {
    // TODO: Add other tachyons classes
    const options = [{
      selector: '.min-vh-100',
      vh: 100,
      min: true
    }]

    this.vhFix = new VHChromeFix(options)
  }

  /* ==========================================================================
     Video controls
     ========================================================================== */

  _initVideos() {
    $('.page.active video').on('click', (e) => this.togglePlayback(e.currentTarget))
  }

  togglePlayback(media, pause) {
    const $media = $(media)
    const $wrapper = $media.closest('.animated-media') || $media
    const isVideo = $media.is('video')
    if (pause || (isVideo && !media.paused)) {
      $wrapper.removeClass('played')
      void Util.reflow($wrapper[0]) // trigger reflow
      $wrapper.addClass('paused')
      if (isVideo) media.pause()
    }
    else {
      $wrapper.removeClass('paused')
      void Util.reflow($wrapper[0]) // trigger reflow
      $wrapper.addClass('played')
      if (isVideo) media.play()
    }
  }

  /* ==========================================================================
     Simple image slideshow
     ========================================================================== */

  _initGalleries(pswp) {
    $('.media-gallery').each((index, gallery) => {
      let paused = false
      let current = 1
      const duration = 600
      const delay = 2000
      const $gallery = $(gallery)
      const $slides = $gallery.find('.gallery-item')
      const $pager = $gallery.find('.gallery-pager')

      $slides.first().removeClass('o-0 z-0').addClass('active z-1')

      function nextSlide($page) {
        for (var i = 0; i < $slides.length; i++) {
          $slides.eq(i).addClass('o-0 z-0').removeClass('active z-1')
          $pager
            .children().eq(i).addClass('o-60')
            .children().removeClass('bg-gold')
        }
        current = $page ? $page.data('slide') : ((current != $slides.length - 1) ? current + 1 : 0);
        $slides.eq(current).removeClass('o-0 z-0').addClass('active z-1')
        $pager
          .children().eq(current).removeClass('o-60')
          .children().addClass('bg-gold')
      }

      const slideshow = setInterval(() => {
        // Dirty way to check if PSWP open
        if (paused || $html.hasClass('pswp-active') || $html.hasClass('loading')) return
        nextSlide()
      }, delay + (duration*2)) // 3000

      $slides.on('click', (e) => {
        paused = !paused
        this.togglePlayback($(e.currentTarget), paused)
      })
      $pager.children('button').on('click', (e) => {
        paused = true
        this.togglePlayback($gallery.find('.animated-media'), paused)
        nextSlide($(e.currentTarget))
      })
    })
  }


  /* ==========================================================================
     AOS scrolling CSS animations
     ========================================================================== */

  _initAOS() {
    AOS.init({
      'duration': 750,
      'anchor-placement': 'center center',
      'once': true
    })
  }

  /* ==========================================================================
     Add class for ajax loading
     ========================================================================== */

  _ajaxLinkClass() {
    const siteURL = 'http://' + top.location.host.toString()
    const $anchorLinks = $(`a[href^="#"]`).not('a[href="#"]')

    // TODO: remove unused classes
    $('.page.active').find('a[href^="' + window.location.origin + '"], a[href^="/"], .project-image a, .project-title a, .project-more a, .project-meta a, .project-tags a, #pagination a')
    .not('[download]')
    .each(function() {
      const link = $(this)

      if (!link.hasClass('rss') && !link.hasClass('pswp-link')) {
        link.addClass('js-ajax-link')

        if (link.attr('href').indexOf('page') > -1) {
          link.addClass('js-archive-index')
        }

        if (link.attr('href') == window.location.origin) {
          link.addClass('js-show-index')
        }

        if (link.attr('href').indexOf('tag') > -1) {
          link.addClass('js-tag-index')
        }

        if (link.attr('href').indexOf('author') > -1) {
          link.addClass('js-author-index')
        }
      }
    })

    // Smooth scrolling links
    $anchorLinks.on('click', e => {
      e.preventDefault()
      this._smoothScrollToElement(e.currentTarget.hash)
    })
  }

  _smoothScrollToElement(element) {
    const elementSelector = document.querySelector(element)
    const elementOffset = elementSelector.getBoundingClientRect().top
    const scrollPosition = window.scrollY
    const documentTop = document.documentElement.clientTop

    const scrollOffset = elementOffset + scrollPosition - documentTop

    anime({
      targets: [document.documentElement, document.body],
      scrollTop: scrollOffset,
      duration: 600,
      easing: 'easeOutCirc'
    })
  }

  /* ==========================================================================
     Ajax Loading
     ========================================================================== */

  _initAjax() {
    const that = this

    window.onpopstate = (e) => {
      const url = e.state && e.state.url
      // Hack to prevent PhotoSwipe from triggering onpopstate page transition
      if ($html.hasClass('pswp-active') || !url) {
        $html.removeClass('pswp-active')
        return
      } else { this._pageRequest(url, true) }
    }

    function followLink($link) {
      if (!that.state.loading) {
        const currentUrl = window.location.pathname
        const url = $link.attr('href')
        const title = $link.attr('title') || null

        // TODO: specific animations for projects
        $link.addClass('active')

        if (url.replace(/\/$/, "") !== currentUrl.replace(/\/$/, "")) {
          that.state.loading = true
          that._pageRequest(url, null, title)
        }
        else {
          that._smoothScrollToElement(`#${that.$ajaxContainer.attr('id')}`)
        }
      }
    }

    $body.on('click', '.js-ajax-link', function(e) {
      e.preventDefault()

      const $link = $(this)
      const $menu = $('#menu')

      if ($link.hasClass('project-nav-item') || $link.hasClass('pagination-item')) {
        if ($link.hasClass('project-nav-next') || $link.hasClass('pagination-next')) {
          that.state.pageDirection = 'next'
        }
        if ($link.hasClass('project-nav-prev') || $link.hasClass('pagination-prev')) {
          that.state.pageDirection = 'prev'
        }
      } else {
        that.state.pageDirection = null
      }

      if ($menu.is(':visible')) {
        $menu.one('hidden.nd.modal', () => followLink($link))
        $menu.modal('hide')
      } else {
        followLink($link)
      }
    })
  }

  _pageRequest(url, pop, title) {
    const that = this
    const $oldRegion = this.$ajaxContainer.find('.page')
    const st = window.pageYOffset || document.documentElement.scrollTop
    const stNew = pop ? st : 0

    if (this._req) this._req.abort()

    function pageRequest() {
      that._req = $.get(url, (result) => that._showPage(result, pop, url, title))
        .fail(() => {
          $.get('404.html', (result) => that._showPage(result, pop, url, title))
        })
    }

    this.$ajaxContainer.addClass('overflow-hidden')
    $html.addClass('loading')
    navAffix.disable()
    this._showLoader()

    if (pop) {
      $body[0].style.height = `${Math.max($oldRegion[0].offsetHeight, this.maxDocHeight)}px`
      $oldRegion.hide() // Can't easily avoid scroll jumps during anime :-(
      pageRequest()
    }
    else {
      // NOTE: `.page` element MUST have a background colour to avoid glitches
      // when changing from static/relative to fixed/absolute position
      // UPDATE: still getting glitches with fixed positioning and scrollTop
      $oldRegion[0].className = pageOutClasses
      $oldRegion.css('transformOrigin', `50% ${st + $(window).height()/2}px`)
      anime({
        duration: pageDuration,
        easing: pageEasing,
        targets: $oldRegion[0],
        scale: 1.1,
        opacity: 0,
        complete: pageRequest
      })
    }
  }

  _showPage(result, pop, url, title) {
    const that = this
    const $result = $(result)
    const newContent = $(`#${this.$ajaxContainer.attr('id')} .page`, $result).contents()
    const newTitle = title || $result.filter('title').text()
    const $newRegion = $(`<div class="page active bg-white ${pageInClasses}"/>`).append(newContent)
    const $oldRegion = this.$ajaxContainer.find('.page')
    const st = window.pageYOffset || document.documentElement.scrollTop
    const stNew = pop ? st : 0

    this._req = null

    if (!pop) {
      history.pushState({ url: ""+url+"" }, newTitle, url)
    }

    this._hideLoader(false)

    this.$ajaxContainer.append($newRegion)

    this._VHChromeFix()

    this.maxDocHeight = Math.max($newRegion[0].scrollHeight, this.maxDocHeight)
    $body[0].style.height = `${this.maxDocHeight}px`

    if (this.rellax) this.rellax.destroy();

    function complete() {
      $newRegion.removeClass(pageInClasses)
      $oldRegion.remove()
      that.$ajaxContainer.removeClass('overflow-hidden')
      if (pop) window.scrollTo(0, stNew) // document.documentElement.scrollTop doesn't work in Safari
      $body[0].style.height = null
      document.title = newTitle // Is this necessary? $('<textarea/>').html(newTitle).text()
      that.state.loading = false
      that._onPageShown($newRegion)
    }

    if (pop) {
      $newRegion[0].scrollTop = stNew
      this._beforePageShow($newRegion) // TODO: trigger when stuff is visible?
      $newRegion.show().removeClass('hidden') // Can't easily avoid rendering glitches when using anime :-(
      complete()
    }
    else {
      window.scrollTo(0, stNew) // document.documentElement.scrollTop doesn't work in Safari
      anime({
        duration: pageDuration,
        easing: pageEasing,
        targets: $newRegion[0],
        scale: [0.88, 1],
        opacity: [0, 1],
        // offset: 0,
        begin: () => {
          $newRegion.addClass('in').removeClass('hidden')
          this._beforePageShow($newRegion) // TODO: trigger when stuff is visible?
        },
        complete: complete
      })
    }
  }


  /* ==========================================================================
     Reload all scripts after AJAX load
     ========================================================================== */

   // Init all immediately visible stuff before page
   // is visible (i.e. loader has faded out)
  _beforePageShow($page) {
    // Add classes to all ajax links
    this._ajaxLinkClass()
    $html.removeClass('loading')
    // Init Rellax parallax
    if ($page.find('.rellax').length) this.rellax = new Rellax('.page.active .rellax')
    // Rebrand the UI if necessary
    this._rebrandUI()
    // Toggle the color of the navbar based on the jumbotron
    navAffix.toggleInverse($page.find('.jumbotron:first'))
    // When images are loaded
    const lazy = $page[0].querySelectorAll('.lazy');
    if (lazy === null) return
    Array.from(lazy).forEach(img => {
      img.addEventListener('load', () => $(img).closest('figure, .video').addClass('loaded'))
    })

    $page.find('.current-year').text(currentYear);
  }

   // Do stuff on page load
   // TODO: consider scope for this function - i.e.
   // would some elements outside of the ajax content
   // get initialized twice?
  _onPageShown($page) {
    // Highlight active menu item
    this._highlightMenuItem()
    // Init custom input labels
    $page.find('.input-with-label').each((i, el) => this._toggleInputValue(el))
    // Init Photoswipe
    if ($page.find('.pswp-figure').length) this.PSWP = new photoSwipe($page)
    // Init gallery
    if ($page.find('.media-gallery').length) this._initGalleries(this.PSWP)
    // Refresh AOS
    AOS.refreshHard()
    // Init video controls
    this._initVideos()
    // Re-enable navbar affix
    navAffix.enable()
    // Reset focus
    if (document.activeElement) document.activeElement.blur()
  }

  /* ==========================================================================
     Form input behaviour
     ========================================================================== */

  _toggleInputValue(input) {
    $(input).toggleClass('has-value', $(input).val().length > 0)
  }

  /* ==========================================================================
     Rebrand the UI
     ========================================================================== */

  _rebrandUI() {
    const currentColor = $('body').data('brand-color')
    if ($('.page.active [data-brand-color]').length) {
      const newColor = $('[data-brand-color]').data('brand-color')
      $('body').data('brand-color', newColor)
      $('.brand-color')
        .removeClass(currentColor)
        .addClass(newColor)
      $('#nav-title')
        .removeClass('gold')
        .addClass('white')
    }
    else {
      $('body').data('brand-color', null)
      $('.brand-color')
        .removeClass(currentColor)
      $('#nav-title')
        .removeClass('white')
        .addClass('gold')
    }
  }
}

// Netlify authentication for CMS
if (window.netlifyIdentity) {
  window.netlifyIdentity.on("init", user => {
    if (!user) {
      window.netlifyIdentity.on("login", () => {
        document.location.href = "/admin/";
      });
    }
  });
}

window.app = new App({ ajaxContainer: '#ajax-container' }).init()
