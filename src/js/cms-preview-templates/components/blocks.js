import React from "react";

export default class Blocks extends React.Component {
  render() {
    const {entry, widgetFor, getAsset} = this.props;

    return (
      <div className="pa3">
        { widgetFor('blocks') }
      </div>
    )
  }
}

// export default class BlocksPreview extends React.Component {
//   // Adapted from ObjectPreview in core
//   render() {
//     const { field } = this.props
//     return <div className="nc-widgetPreview">{( field && field.get('fields') || null)}</div>
//   }
// }