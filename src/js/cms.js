import React from "react";
import CMS from "netlify-cms";

import HomePreview from "./cms-preview-templates/home";
import PostPreview from "./cms-preview-templates/post";
import ProductsPreview from "./cms-preview-templates/products";
import ValuesPreview from "./cms-preview-templates/values";
import ContactPreview from "./cms-preview-templates/contact";
// import { BlockSelect, BlockPreview, ListyControl } from "./cms-widgets/Blocks";
import BlocksControl from "./cms-widgets/Blocks";
import BlocksPreview from "./cms-preview-templates/components/blocks";
// Editor widgets (Shortcodes)
import Figure from "./cms-widgets/Figure"
import YouTube from "./cms-widgets/YouTube"
import Gallery from "./cms-widgets/Gallery"


// Example of creating a custom color widget
class ColorControl extends React.Component {
  render() {
    return <input
      style={{height: "80px"}}
      type="color"
      value={this.props.value}
      onInput={(e) => this.props.onChange(e.target.value)}
    />
  }
}

CMS.registerPreviewStyle("../css/main.css");
CMS.registerPreviewTemplate("home", HomePreview);
CMS.registerPreviewTemplate("post", PostPreview);
CMS.registerPreviewTemplate("products", ProductsPreview);
CMS.registerPreviewTemplate("values", ValuesPreview);
CMS.registerPreviewTemplate("contact", ContactPreview);

CMS.registerWidget("color", ColorControl);
CMS.registerWidget("blocks", BlocksControl, CMS.getWidget('object').preview);

const plugins = [
  Figure,
  YouTube,
  Gallery
]
plugins.forEach(CMS.registerEditorComponent)
